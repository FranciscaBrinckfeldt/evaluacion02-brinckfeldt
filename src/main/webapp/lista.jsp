<%-- 
    Document   : lista.jsp
    Created on : 18-04-2020, 20:03:02
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Listado de Productos</title>
    </head>
    <body>
        
        <div class="generalcontainer">
            <h3 class="title is-3" align="center">Listado de Productos</h3>
        
            <div class="buttoncontainer">
                <a href="crear" class="button is-success" align="center">Registrar nuevo producto</a>
            </div>


            <div class="columns is-mobile">
                <div class="column is-8 is-offset-2">
                    <table class="table is-fullwidth">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Precio</th>
                            <th>Stock</th>
                            <th>Descripción</th>
                            <th colspan="2">Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${productos}" var="producto">
                                <tr>
                                    <td><c:out value="${producto.idProducto}"/></td>
                                    <td><c:out value="${producto.nombreProducto}"/></td>
                                    <td><c:out value="${producto.marcaProducto}"/></td>
                                    <td><c:out value="$${producto.precioProducto}"/></td>
                                    <td><c:out value="${producto.stockProducto}"/></td>
                                    <td><c:out value="${producto.descripcionProducto}"/></td>
                                    <td><a href="editar?id=${producto.idProducto}" class="button is-warning is-small">Editar</a></td>
                                    <td><a href="eliminar?id=${producto.idProducto}" class="button is-danger is-small">Eliminar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
