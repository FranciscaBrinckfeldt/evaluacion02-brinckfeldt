<%-- 
    Document   : editar.jsp
    Created on : 18-04-2020, 20:04:23
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Editando un Producto</title>
    </head>
    <body>
        
        <%
            int id = (int) request.getAttribute("id");
            String nombre = (String) request.getAttribute("nombre");
            String marca = (String) request.getAttribute("marca");
            int precio = (int) request.getAttribute("precio");
            int stock = (int) request.getAttribute("stock");
            String descripcion = (String) request.getAttribute("descripcion");
        %>
        
        <div class="generalcontainer">
            
            <h3 class="title is-3" align="center">Modifica los nuevos datos del producto:</h3>
        
            <form name="form" action="editar" method="POST">
                <div class="container">
                    <div class="columns is-mobile">
                        <div class="column is-4 is-offset-4">

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">ID:</label>
                                    <div class="control">
                                        <input class="input" type="text" name="id" value="<%=id%>" readonly>
                                    </div>  
                                </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Nombre:</label>
                                    <div class="control">
                                      <input class="input" type="text" name="nombre" value="<%=nombre%>">
                                    </div>  
                                </div>
                             </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Marca:</label>
                                    <div class="control">
                                      <input class="input" type="text" name="marca" value="<%=marca%>">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <label class="label">Precio:</label>
                                <div class="field has-addons">
                                    <div class="control">
                                        <span class="button is-static">
                                            $
                                        </span>
                                    </div>
                                    <div class="control" style="width: 100%;">
                                      <input class="input" type="number" name="precio" value="<%=precio%>">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Stock</label>
                                    <div class="control">
                                      <input class="input" type="number" name="stock" value="<%=stock%>">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Descripción</label>
                                    <div class="control">
                                      <input class="input" type="text" name="descripcion" value="<%=descripcion%>">
                                    </div>
                                 </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <div class="buttonform">
                                        <input class="button is-primary is-fullwidth" type="submit" value="Editar">
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div> 
    </body>
</html>
