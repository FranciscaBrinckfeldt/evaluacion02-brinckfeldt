<%-- 
    Document   : crear.jsp
    Created on : 18-04-2020, 20:04:14
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">   
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Creando un Producto</title>
    </head>
    <body>
        
        <div class="generalcontainer">
            
            <h3 class="title is-3" align="center">Ingresa los datos del nuevo producto:</h3>
        
            <form name="form" action="crear" method="POST">
                <div class="container">
                    <div class="columns is-mobile">
                        <div class="column is-4 is-offset-4">

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">ID:</label>
                                    <div class="control">
                                        <input class="input" type="text" name="id">
                                    </div>  
                                </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Nombre:</label>
                                    <div class="control">
                                      <input class="input" type="text" name="nombre">
                                    </div>  
                                </div>
                             </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Marca:</label>
                                    <div class="control">
                                      <input class="input" type="text" name="marca">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <label class="label">Precio:</label>
                                <div class="field has-addons">
                                    <div class="control">
                                        <span class="button is-static">
                                            $
                                        </span>
                                    </div>
                                    <div class="control" style="width: 100%;">
                                      <input class="input" type="number" name="precio">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Stock</label>
                                    <div class="control">
                                      <input class="input" type="number" name="stock">
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Descripción</label>
                                    <div class="control">
                                      <input class="input" type="text" name="descripcion">
                                    </div>
                                 </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <div class="buttonform">
                                        <input class="button is-primary is-fullwidth" type="submit" value="Registrar">
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
