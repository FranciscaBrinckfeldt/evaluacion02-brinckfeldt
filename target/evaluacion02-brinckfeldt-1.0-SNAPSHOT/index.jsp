<%-- 
    Document   : index.jsp
    Created on : 18-04-2020, 19:50:34
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Bienvenido</title>
    </head>
    <body>
        <div class="generalcontainer">
            <h1 class="title is-1" align="center">¡Bienvenido/a!</h1>

            <div class="content">
                <p align="center">En esta página podrás ver el stock de los productos de tu negocio.</p>
            </div>

            <h4 class="title is-4" align="center">Haz click en el siguiente botón para ver la lista de los productos:</h4>

            <div class="buttoncontainer">
                <a href="lista" class="button is-primary" align="center">Listado de productos</a>
            </div>
        </div>        
    </body>
</html>
